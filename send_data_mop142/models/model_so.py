# -*- coding: utf-8 -*-

from odoo import api, fields, models
import requests
import json


class payment(models.Model):
    _inherit = 'account.payment'

    is_send_14 = fields.Boolean(string="", )
    is_send_14_payment = fields.Boolean(string="", )

    def get_partner(self, partner_id):
        data = {
            "name": partner_id.name,
            "mobile": partner_id.mobile,
            "email": partner_id.email,
            "national_id": partner_id.x_national_id,
        }
        return data

    def get_category_data(self, categ):
        data = {
            'id': categ.id,
            'name': str(categ.name).replace("'", "").replace('"', ''),
        }
        if categ.parent_id:
            data['parent'] = self.get_category_data(categ.parent_id)
        return data

    def get_category_uom_data(self, category_uom):
        data = {
            'id': category_uom.id,
            'name': str(category_uom.name).replace("'", "").replace('"', ''),
        }
        return data

    def gat_uom_data(self, uom):
        data = {
            'id': uom.id,
            'name': str(uom.name).replace("'", "").replace('"', ''),
            'category': self.get_category_uom_data(uom.category_id)
        }
        return data

    def get_product_data(self, product):
        data = {
            'id': product.id,
            'name': str(product.name).replace("'", "").replace('"', ''),
            'type': str(product.type).replace("'", "").replace('"', ''),
            'product_code': str(product.default_code).replace("'", "").replace('"', ''),
            'barcode': str(product.barcode).replace("'", "").replace('"', ''),
            'category': self.get_category_data(product.categ_id),
            'sale_price': product.lst_price,
            'uom': self.gat_uom_data(product.uom_id),
        }
        return data

    def get_tax_data(self, tax):
        data = {
            'id': tax.id,
            'name': str(tax.name).replace("'", "").replace('"', ''),
            'type': str(tax.type_tax_use).replace("'", "").replace('"', ''),
            'amount_type': str(tax.amount_type or "percent").replace("'", "").replace('"', ''),
            'amount': str(tax.amount).replace("'", "").replace('"', ''),
        }
        return data

    def get_lines_data(self, lines):
        data = []
        for line in lines:
            data.append(
                {
                    'id': line.id,
                    'product': self.get_product_data(line.product_id),
                    'tax_id': self.get_tax_data(line.invoice_line_tax_ids),
                    'quantity': line.quantity,
                    'price_unit': line.price_unit,
                    'description': str(line.name).replace("'", "").replace('"', ''),
                }
            )
        return data

    def get_payments_data(self):
        # data = []
        # print('inv_name', inv_name)
        # payments = self.env['account.payment'].sudo().search([('communication', '=', inv_name)])
        # print('payments', payments.mapped('name'))
        for payment in self:
            return {
                'id': payment.id,
                'name': payment.name,
                'payment_type': payment.payment_type,
                'payment_amount': payment.amount,
                'payment_date': payment.payment_date,
                'state': payment.state,
                "partner_id": self.get_partner(payment.partner_id),
                "journal_name": payment.journal_id.id,
            }

    def send_payment_to_14(self):
        print('send_payment_to_14')
        payments = self.search(
            [('is_send_14_payment', '=', False), ('payment_type', '!=', 'transfer'), ('journal_id', "not in", [19])],
            limit=500)
        data = []
        print('payments', payments)
        for rec in payments:
            inv_dec = rec.get_payments_data()
            # {
            #     'id': rec.id,
            #     "name": rec.number,
            #     "state": rec.state,
            #     "date_invoice": rec.date_invoice,
            #     "partner_id": rec.get_partner(rec.partner_id),
            #     'lines': rec.get_lines_data(rec.invoice_line_ids),
            #     'payments': []
            # }
            # if rec.state in ('open', 'paid'):
            #     inv_dec['payments'] = rec.get_payments_data(rec.number)
            if inv_dec:
                data.append(
                    inv_dec
                )
        url = "https://egypt-trust.odoo.com/api/create/payments"
        params = {
            "params": {
                'data': data
            }
        }
        # print("pamynt", json.dumps(params))
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        response = requests.request('POST', url, data=json.dumps(params), headers=headers)
        # if rec.number == response.json().get('result'):
        print('response', response)
        if response.status_code == 200:
            print("response.json().get('result', []))", response.json())
            self.search([('name', 'in', response.json().get('result', []))]).update({'is_send_14_payment': True})
            print("Done payment")
            # payments.update({'is_send_14_payment': True})


class invoice(models.Model):
    _inherit = 'account.invoice'

    is_send_14 = fields.Boolean(string="", )
    is_send_14_invoice = fields.Boolean(string="", default=False)

    def get_partner(self, partner_id):
        data = {
            "name": partner_id.name,
            "mobile": partner_id.mobile,
            "email": partner_id.email,
            "national_id": partner_id.x_national_id,
        }
        return data

    def get_category_data(self, categ):
        data = {
            'id': categ.id,
            'name': str(categ.name).replace("'", "").replace('"', ''),
        }
        if categ.parent_id:
            data['parent'] = self.get_category_data(categ.parent_id)
        return data

    def get_category_uom_data(self, category_uom):
        data = {
            'id': category_uom.id,
            'name': str(category_uom.name).replace("'", "").replace('"', ''),
        }
        return data

    def gat_uom_data(self, uom):
        data = {
            'id': uom.id,
            'name': str(uom.name).replace("'", "").replace('"', ''),
            'category': self.get_category_uom_data(uom.category_id)
        }
        return data

    def get_product_data(self, product):
        data = {
            'id': product.id,
            'name': str(product.name).replace("'", "").replace('"', ''),
            'type': str(product.type).replace("'", "").replace('"', ''),
            'product_code': str(product.default_code).replace("'", "").replace('"', ''),
            'barcode': str(product.barcode).replace("'", "").replace('"', ''),
            'category': self.get_category_data(product.categ_id),
            'sale_price': product.lst_price,
            'uom': self.gat_uom_data(product.uom_id),
        }
        return data

    def get_tax_data(self, tax):
        data = {
            'id': tax.id,
            'name': str(tax.name).replace("'", "").replace('"', ''),
            'type': "sale",
            'amount_type': str(tax.amount_type or "percent").replace("'", "").replace('"', ''),
            'amount': str(tax.amount).replace("'", "").replace('"', ''),
        }
        return data

    def get_lines_data(self, lines):
        data = []
        for line in lines:
            data.append(
                {
                    'id': line.id,
                    'product': self.get_product_data(line.product_id),
                    'tax_id': self.get_tax_data(line.invoice_line_tax_ids),
                    'quantity': line.quantity,
                    'price_unit': line.price_unit,
                    'description': str(line.name).replace("'", "").replace('"', ''),
                }
            )
        return data

    def get_payments_data(self, inv_name):
        data = []
        # print('inv_name', inv_name)
        # payments = self.env['account.payment'].sudo().search([('communication', '=', inv_name)])
        # print('payments', payments.mapped('name'))
        # for payment in payments:
        #     data.append(
        #         {
        #             'id': payment.id,
        #             'name': payment.name,
        #             'payment_type': payment.payment_type,
        #             'payment_amount': payment.amount,
        #             'payment_date': payment.payment_date,
        #             'state': payment.state,
        #             "partner_id": self.get_partner(payment.partner_id),
        #             "journal_name": payment.journal_id.id,
        #         }
        #     )
        return data

    def send_data_to_14(self):
        print('send_data_to_14_invoice')
        invoices = self.search([('is_send_14_invoice', '=', False),
                                ('state', 'in', ('open', 'paid'))], limit=50)
        data = []
        print("invoices", invoices)
        for rec in invoices:
            inv_dec = {
                'id': rec.id,
                "name": rec.number,
                "state": rec.state,
                "date_invoice": rec.date_invoice,
                "partner_id": rec.get_partner(rec.partner_id),
                'lines': rec.get_lines_data(rec.invoice_line_ids),
                'payments': []
            }
            # print("rec.number",rec.number)
            # if rec.state in ('open', 'paid'):
            #     inv_dec['payments'] = rec.get_payments_data(rec.number)
            if inv_dec:
                data.append(
                    inv_dec
                )
        url = "https://egypt-trust.odoo.com/api/create/invoices"
        params = {
            "params": {
                'data': data
            }
        }
        # print("params", json.dumps(params))
        print("DAta")
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        response = requests.request('POST', url, data=json.dumps(params), headers=headers)
        # if rec.number == response.json().get('result'):
        print('response', response)
        if response.status_code == 200:
            print("response.json().get('result', []", response.json().get('result', []))
            self.search([('number', 'in', response.json().get('result', []))]).update({'is_send_14_invoice': True})
            print('Done IN')

            # invoices.update({'is_send_14_invoice': True})


class sale_order(models.Model):
    _inherit = 'sale.order'

    is_send_14 = fields.Boolean(string="", )

    #
    # def read_unicode(self,text, charset='utf-8'):
    #     if isinstance(text, basestring):
    #         if not isinstance(text, unicode):
    #             text = unicode(text, charset)
    #     return text

    def get_partner(self, partner_id):
        data = {
            "name": partner_id.name,
            "mobile": partner_id.mobile,
            "email": partner_id.email,
        }
        return data

    def get_category_data(self, categ):
        data = {
            'id': categ.id,
            'name': str(categ.name).replace("'", "").replace('"', ''),
        }
        if categ.parent_id:
            data['parent'] = self.get_category_data(categ.parent_id)
        return data

    def get_category_uom_data(self, category_uom):
        data = {
            'id': category_uom.id,
            'name': str(category_uom.name).replace("'", "").replace('"', ''),
        }
        return data

    def gat_uom_data(self, uom):
        data = {
            'id': uom.id,
            'name': str(uom.name).replace("'", "").replace('"', ''),
            'category': self.get_category_uom_data(uom.category_id)
        }
        return data

    def get_product_data(self, product):
        data = {
            'id': product.id,
            'name': str(product.name).replace("'", "").replace('"', ''),
            'type': str(product.type).replace("'", "").replace('"', ''),
            'product_code': str(product.default_code).replace("'", "").replace('"', ''),
            'barcode': str(product.barcode).replace("'", "").replace('"', ''),
            'category': self.get_category_data(product.categ_id),
            'sale_price': product.lst_price,
            'uom': self.gat_uom_data(product.uom_id),
        }
        return data

    def get_tax_data(self, tax):
        data = {
            'id': tax.id,
            'name': str(tax.name).replace("'", "").replace('"', ''),
            'type': "sale",
            'amount_type': str(tax.amount_type or "percent").replace("'", "").replace('"', ''),
            'amount': str(tax.amount).replace("'", "").replace('"', ''),
        }
        return data

    def get_lines_data(self, lines):
        data = []
        for line in lines:
            data.append(
                {
                    'id': line.id,
                    'product': self.get_product_data(line.product_id),
                    'tax_id': self.get_tax_data(line.tax_id),
                    'quantity': line.product_uom_qty,
                    'price_unit': line.price_unit,
                    'description': str(line.name).replace("'", "").replace('"', ''),
                }
            )
        return data

    def send_data_to_14(self):
        # return True
        print("send_data_to_14")
        orders = self.search([('is_send_14', '=', False), ('state', '=', 'sale')], limit=200)
        data = []
        for rec in orders:
            data.append(
                {
                    "name": rec.name,
                    "confirmation_date": rec.date_order,
                    "partner_id": rec.get_partner(rec.partner_id),
                    'lines': rec.get_lines_data(rec.order_line),
                    'currency_id': rec.currency_id.name
                }
            )

        url = "https://egypt-trust.odoo.com/api/create/orders"
        params = {
            "params": {
                'data': data
            }
        }
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        print('data')
        response = requests.request('POST', url, data=json.dumps(params), headers=headers, verify=False)
        print("response", response)
        if response.status_code == 200:
            # print(" response.json().get('result',[]))", response.json().get('result', []))
            self.search([('name', 'in', response.json().get('result', []))]).update({'is_send_14': True})
            print("Done SO")


class soso(models.Model):
    _name = 'so.so'
    _rec_name = 'name'
    _description = 'New Description'

    name = fields.Char()

    @api.model
    def create(self, vals):
        res = super(soso, self).create(vals)
        res.is_send_14 = True
        return res


class account_move(models.Model):
    _inherit = 'account.move'

    is_send_to_move_14 = fields.Boolean(string="", )

    def get_partner(self, partner_id):
        data = {
            "name": partner_id.name,
            "mobile": partner_id.mobile,
            "email": partner_id.email,
            "national_id": partner_id.x_national_id,
        }
        return data

    def get_lines_data(self):
        data = []
        for line in self.line_ids:
            data.append(
                {
                    'id': line.id,
                    'account': line.account_id.code,
                    'partner_id': self.get_partner(line.partner_id),
                    'name': line.name,
                    'debit': line.debit,
                    'credit': line.credit,
                }
            )
        return data

    def send_journal_to_14(self):
        # return True
        print("send_journal_to_14")
        orders = self.search([('is_send_to_move_14', '=', False), ('journal_id', 'not in', (1, 2) )], limit=500)
        data = []
        for rec in orders:
            data.append(
                {
                    "name": rec.name,
                    "date": rec.date,
                    "ref": rec.ref,
                    "journal_id": rec.journal_id.id,
                    'lines': rec.get_lines_data(),
                    'state': rec.state,
                }
            )

        url = "https://egypt-trust.odoo.com/api/create/journal"
        params = {
            "params": {
                'data': data
            }
        }
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        print('data',json.dumps(params))
        print('data')
        response = requests.request('POST', url, data=json.dumps(params), headers=headers, verify=False)
        print("response", response)
        if response.status_code == 200:
            print(" response.json().get('result',[]))", response.json().get('result', []))
            self.search([('name', 'in', response.json().get('result', []))]).update({'is_send_to_move_14': True})
            print("Done journal")


class partner(models.Model):
    _inherit = 'res.partner'

    is_update = fields.Boolean(string="",  )
    is_send = fields.Boolean(string="",  )

    def send_branch_partner(self):
        print("send_branch_partner")
        orders = self.search([('is_send', '=', False),('branch_ids', '!=', False)], limit=4000)
        data = []
        for rec in orders:
            data.append(
                {
                    "name": rec.name,
                    'branch': rec.branch_ids.name,
                }
            )

        url = "https://egypt-trust.odoo.com/api/update/partner"
        params = {
            "params": {
                'data': data
            }
        }
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        # print('data',json.dumps(params))
        print('data')
        response = requests.request('POST', url, data=json.dumps(params), headers=headers, verify=False)
        print("response", response)
        if response.status_code == 200:
            orders.update({'is_send': True})
            print(" response.json().get('result',[]))", response.json())
            self.search([('name', 'in', response.json().get('result', []))]).update({'is_update': True})
            print("Done partner")

