from odoo import models, fields, api, _
import requests
import json
from urllib import urlencode
import urllib2

class purchase_requisition(models.Model):
    _inherit = 'purchase.requisition'

    def get_user_data(self, user):
        data = {
            'id': user.id,
            'name': str(user.name).replace("'","").replace('"',''),
            'login': str(user.login).replace("'","").replace('"',''),
        }
        return data

    def get_project_data(self, project):
        data = {
            'id':project.id,
            'name':str(project.name).replace("'","").replace('"',''),
            'code':str(project.abbr).replace("'","").replace('"',''),
        }
        return data

    def get_milestone_data(self, milestone):
        data = {
            'id':milestone.id,
            'name':str(milestone.name).replace("'","").replace('"',''),
            'milestone_code':str(milestone.milestone_code).replace("'","").replace('"',''),
            'project':self.get_project_data(milestone.project_id),
        }
        return data

    def get_task_data(self, task):
        data = {
            'id':task.id,
            'name':str(task.name).replace("'","").replace('"',''),
            'task_code':str(task.task_code).replace("'","").replace('"',''),
            'project':self.get_project_data(task.project_id),
            'assigned_to':self.get_user_data(task.user_id),
            'milestone':self.get_milestone_data(task.milestone_id)
        }
        return data

    def get_sub_task_data(self, sub_task):
        data = data = {
            'id':sub_task.id,
            'task':str(sub_task.task_ref).replace("'","").replace('"',''),
            'name':str(sub_task.name).replace("'","").replace('"',''),
            'task_code':str(sub_task.subtask_code).replace("'","").replace('"',''),
            'project':self.get_project_data(sub_task.project_id),
            'assigned_to':self.get_user_data(sub_task.assigned_user),
            'milestone':self.get_milestone_data(sub_task.milestone_id)
        }
        return data

    def get_cost_center_data(self, analytic_account):
        data = {
            'id':analytic_account.id,
            'name':str(analytic_account.name).replace("'","").replace('"',''),
            'code':str(analytic_account.code).replace("'","").replace('"',''),
        }
        return data

    def get_project_center_data(self, analytic_tags_ids):
        data = []
        for tag in analytic_tags_ids:
            data.append(
                {
                    'id':tag.id,
                    'name':str(tag.name).replace("'","").replace('"','')
                }
            )
        return data

    def get_category_data(self,categ):
        data={
            'id':categ.id,
            'name':str(categ.name).replace("'","").replace('"',''),
        }
        if categ.parent_id:
            data['parent']=self.get_category_data(categ.parent_id)
        return data

    def gat_uom_data(self,uom):
        data={
            'id':uom.id,
            'name':str(uom.name).replace("'","").replace('"',''),
            'category':self.get_category_uom_data(uom.category_id)
        }
        return data

    def get_category_uom_data(self,category_uom):
        data={
            'id':category_uom.id,
            'name':str(category_uom.name).replace("'","").replace('"',''),
        }
        return data

    def get_product_data(self,product):
        data={
            'id':product.id,
            'name':str(product.name).replace("'","").replace('"',''),
            'type':str(product.type).replace("'","").replace('"',''),
            'product_code':str(product.default_code).replace("'","").replace('"',''),
            'barcode':str(product.barcode).replace("'","").replace('"',''),
            'category':self.get_category_data(product.categ_id),
            'sale_price':product.lst_price,
            'uom':self.gat_uom_data(product.uom_id),
        }
        return data


    def get_lines_data(self, lines):
        data = []
        for line in lines:
            data.append(
                {
                    'id':line.id,
                    'product':self.get_product_data(line.product_id),
                    'quantity':line.product_qty,
                    'description':str(line.description).replace("'","").replace('"',''),
                }
            )
        return data

    @api.multi
    def send_to_mop_14(self):
        data = []
        for rec in self:
            state=rec.state
            if state=='cancel':
                state='rejected'
            rec_data={
                'id': rec.id,
                'state': state,
                'name': str(rec.name).replace("'","").replace('"',''),
                'requested_by': rec.get_user_data(rec.user_id),
                'approver ': rec.get_user_data(rec.approver_id),
                'purchase_type': str(rec.purchase_type).replace("'","").replace('"',''),
                'buyer': rec.get_user_data(rec.notification_to),
                'no_need_f_technical_approve': str(rec.tech_app_re).replace("'","").replace('"',''),
                'project': rec.get_project_data(rec.project_id),
                'milestone': rec.get_milestone_data(rec.milestones_id),
                'task': rec.get_task_data(rec.task_id),
                'sub_task': rec.get_sub_task_data(rec.subtasks_id),
                'lines': rec.get_lines_data(rec.line_ids)
            }
            if rec.line_ids:
                rec_data['cost_center']=rec.get_cost_center_data(
                    rec.line_ids[0].analytic_account_id)
                rec_data['project_center']=rec.get_project_center_data(
                    rec.line_ids[0].analytic_tags_ids)
            data.append(rec_data)
        url = "http://138.68.100.214:8014/api/get/data/mop10"
        params={
            "params": {
                'data':data
            }
        }
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        response = requests.request('POST', url, data=json.dumps(params),headers=headers, verify=False)
        print('response',response)

class purchase_order(models.Model):
    _inherit = 'purchase.order'

    def get_currency_data(self, partner):
        data = {
            'id': partner.id,
            'name': str(partner.name).replace("'","").replace('"',''),
            'symbol': str(partner.symbol).replace("'","").replace('"',''),
            'rate': partner.rate,
            'rounding': partner.rounding,
            'decimal_places ': partner.decimal_places,
        }
        return data

    def get_partner_data(self, partner):
        data = {
            'id': partner.id,
            'name': str(partner.name).replace("'","").replace('"',''),
        }
        return data

    def get_user_data(self, user):
        data = {
            'id': user.id,
            'name': str(user.name).replace("'","").replace('"',''),
            'login': str(user.login).replace("'","").replace('"',''),
        }
        return data

    def get_project_data(self, project):
        data = {
            'id':project.id,
            'name':str(project.name).replace("'","").replace('"',''),
            'code':str(project.abbr).replace("'","").replace('"',''),
        }
        return data

    def get_milestone_data(self, milestone):
        data = {
            'id':milestone.id,
            'name':str(milestone.name).replace("'","").replace('"',''),
            'milestone_code':str(milestone.milestone_code).replace("'","").replace('"',''),
            'project':self.get_project_data(milestone.project_id),
        }
        return data

    def get_task_data(self, task):
        data = {
            'id':task.id,
            'name':str(task.name).replace("'","").replace('"',''),
            'task_code':str(task.task_code).replace("'","").replace('"',''),
            'project':self.get_project_data(task.project_id),
            'assigned_to':self.get_user_data(task.user_id),
            'milestone':self.get_milestone_data(task.milestone_id)
        }
        return data

    def get_sub_task_data(self, sub_task):
        data = data = {
            'id':sub_task.id,
            'task':str(sub_task.task_ref).replace("'","").replace('"',''),
            'name':str(sub_task.name).replace("'","").replace('"',''),
            'task_code':str(sub_task.subtask_code).replace("'","").replace('"',''),
            'project':self.get_project_data(sub_task.project_id),
            'assigned_to':self.get_user_data(sub_task.assigned_user),
            'milestone':self.get_milestone_data(sub_task.milestone_id)
        }
        return data

    def get_cost_center_data(self, analytic_account):
        data = {
            'id':analytic_account.id,
            'name':str(analytic_account.name).replace("'","").replace('"',''),
            'code':str(analytic_account.code).replace("'","").replace('"',''),
        }
        return data

    def get_project_center_data(self, analytic_tags_ids):
        data = []
        for tag in analytic_tags_ids:
            data.append(
                {
                'id':tag.id,
                'name':str(tag.name).replace("'","").replace('"','')
                }
            )
        return data

    def get_category_data(self,categ):
        data={
            'id':categ.id,
            'name':str(categ.name).replace("'","").replace('"',''),
        }
        if categ.parent_id:
            data['parent']=self.get_category_data(categ.parent_id)
        return data

    def gat_uom_data(self,uom):
        data={
            'id':uom.id,
            'name':str(uom.name).replace("'","").replace('"',''),
            'category':self.get_category_uom_data(uom.category_id)
        }
        return data

    def get_category_uom_data(self,category_uom):
        data={
            'id':category_uom.id,
            'name':str(category_uom.name).replace("'","").replace('"',''),
        }
        return data

    def get_product_data(self,product):
        data={
            'id':product.id,
            'name':str(product.name).replace("'","").replace('"',''),
            'type':str(product.type).replace("'","").replace('"',''),
            'product_code':str(product.default_code).replace("'","").replace('"',''),
            'barcode':str(product.barcode).replace("'","").replace('"',''),
            'category':self.get_category_data(product.categ_id),
            'sale_price':product.lst_price,
            'uom':self.gat_uom_data(product.uom_id),
        }
        return data


    def get_lines_data(self, lines):
        data = []
        for line in lines:
            data.append(
                {
                    'id':line.id,
                    'product':self.get_product_data(line.product_id),
                    'quantity':line.product_qty,
                    'price_unit':line.price_unit,
                    'description':str(line.name).replace("'","").replace('"',''),
                }
            )
        return data

    @api.multi
    def send_to_mop_14(self):
        print('2222222222222222222222222')
        data = []
        for rec in self:
            state=rec.state
            if state=='cancel':
                state='rejected'
            rec_data={
                'id': rec.id,
                'state': state,
                'name': str(rec.name).replace("'","").replace('"',''),
                'supplier': rec.get_partner_data(rec.partner_id),
                'currency': rec.get_currency_data(rec.currency_id),
                'requisition': rec.requisition_id.id,
                # 'approver ': rec.get_user_data(rec.approver_id),
                'purchase_type': str(rec.purchase_type).replace("'","").replace('"',''),
                # 'buyer': rec.get_user_data(rec.notification_to),
                # 'no_need_f_technical_approve': str(rec.tech_app_re).replace("'","").replace('"',''),
                'project': rec.get_project_data(rec.project_id),
                # 'milestone': rec.get_milestone_data(rec.milestones_id),
                'task': rec.get_task_data(rec.task_id),
                # 'sub_task': rec.get_sub_task_data(rec.subtasks_id),
                'lines': rec.get_lines_data(rec.order_line)
            }
            # if rec.order_line:
            #     rec_data['cost_center']=rec.get_cost_center_data(
            #         rec.order_line[0].analytic_account_id)
            #     rec_data['project_center']=rec.get_project_center_data(
            #         rec.order_line[0].analytic_tags_ids)
            print('rec.requisition_id',rec.requisition_id)
            rec.requisition_id.send_to_mop_14()
            data.append(rec_data)
        url = "http://138.68.100.214:8014/api/get_po/data/mop10"
        params={
            "params": {
                'data':data
            }
        }
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        response = requests.request('POST', url, data=json.dumps(params),headers=headers, verify=False)
        print('PO',response)






